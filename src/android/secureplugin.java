package org.apache.cordova.plugin;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.security.keystore.UserNotAuthenticatedException;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

import javax.crypto.*;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import android.util.Log;

public class secureplugin extends CordovaPlugin {
    /**
     * Alias for our key in the Android Key Store.
     */
    private static final String KEY_NAME = "ceci est la cle de l'enclos des pandas";
    private static final byte[] SECRET_BYTE_ARRAY = new byte[]{1, 2, 3, 4, 5, 6};

    private static final int REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS = 1;

    /**
     * If the user has unlocked the device Within the last this number of seconds,
     * it can be considered as an authenticator.
     */
    private static final int AUTHENTICATION_DURATION_SECONDS = 30;

    private KeyguardManager mKeyguardManager;
    private boolean logged = false;
    private CallbackContext callbackContext;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
              // your init code here
              if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                mKeyguardManager = (KeyguardManager) cordova.getActivity().getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
            }    
        // Send no result, to execute the callbacks later
        PluginResult pluginResult = new  PluginResult(PluginResult.Status.NO_RESULT);
        pluginResult.setKeepCallback(true); // Keep callback
    }

    public void testSecure() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            mKeyguardManager = (KeyguardManager) cordova.getActivity().getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
            assert mKeyguardManager != null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (mKeyguardManager.isKeyguardSecure()) {
                    try {
                        createKey();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (NoSuchProviderException e) {
                        e.printStackTrace();
                    } catch (InvalidAlgorithmParameterException e) {
                        e.printStackTrace();
                    } catch (KeyStoreException e) {
                        e.printStackTrace();
                    } catch (CertificateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        tryEncrypt();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (KeyStoreException e) {
                        e.printStackTrace();
                    } catch (CertificateException e) {
                        e.printStackTrace();
                    } catch (UnrecoverableKeyException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    }
                } else {
                    this.alert("Pour utiliser Pandalab vous devez activer une méthode de devérouillage sécurisé de votre smartphone\n\n" +
                            "Pour cela allez dans : \n" +
                            "Paramétres -> Sécurité -> Ecran de dévérouillage\n\n" +
                            "Ensuite cliquez sur \"c'est bon ! \"", "Alerte de sécurité", "C'est bon !", callbackContext);
                }
            } else  {
                if(!logged)
                showAuthenticationScreen();
            }
        }
    }

    @Override
    public void onResume(boolean multitasking) {
        testSecure();
    }

    /**
     * Tries to encrypt some data with the generated key in {@link #createKey} which is
     * only works if the user has just authenticated via device credentials.
     */
    private boolean tryEncrypt() throws BadPaddingException, IllegalBlockSizeException, KeyStoreException, CertificateException, UnrecoverableKeyException, IOException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
                keyStore.load(null);
                SecretKey secretKey = (SecretKey) keyStore.getKey(KEY_NAME, null);
                Cipher cipher = Cipher.getInstance(
                        KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/"
                                + KeyProperties.ENCRYPTION_PADDING_PKCS7);

                // Try encrypting something, it will only work if the user authenticated within
                // the last AUTHENTICATION_DURATION_SECONDS seconds.
                cipher.init(Cipher.ENCRYPT_MODE, secretKey);
                cipher.doFinal(SECRET_BYTE_ARRAY);

                return true;
            } catch (UserNotAuthenticatedException e) {
                // User is not authenticated, let's authenticate with device credentials.
                showAuthenticationScreen();
                return false;
            } catch (KeyPermanentlyInvalidatedException e) {
                // This happens if the lock screen has been disabled or reset after the key was
                // generated after the key was generated.
                Toast.makeText(cordova.getActivity().getApplicationContext(), "Votre smartphone n'est plus sécurisé, veuillez définir une méthode de dévérrouillage",
                        Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        logged=true;
        if (requestCode == REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS) {
            // Challenge completed, proceed with using cipher
            if (resultCode == cordova.getActivity().RESULT_OK) {
                logged = true;
            } else {
                logged = false;
                this.alert("Pour utiliser Pandalab vous devez activer une méthode de devérouillage sécurisé de votre smartphone\n\n" +
                        "Pour cela allez dans : \n" +
                        "Paramétres -> Sécurité -> Ecran de dévérouillage\n\n" +
                        "Ensuite cliquez sur \"c'est bon ! \"", "Alerte de sécurité", "C'est bon !", callbackContext);
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Creates a symmetric key in the Android Key Store which can only be used after the user has
     * authenticated with device credentials within the last X seconds.
     */
    private void createKey() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, KeyStoreException, CertificateException, IOException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // Generate a key to decrypt payment credentials, tokens, etc.
            // This will most likely be a registration step for the user when they are setting up your app.
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            KeyGenerator keyGenerator = KeyGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            // Set the alias of the entry in Android KeyStore where the key will appear
            // and the constrains (purposes) in the constructor of the Builder
            keyGenerator.init(new KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    // Require that the user has unlocked in the last 30 seconds
                    .setUserAuthenticationValidityDurationSeconds(AUTHENTICATION_DURATION_SECONDS)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        }
    }

    private void showAuthenticationScreen() {
        // Create the Confirm Credentials screen. You can customize the title and description. Or
        // we will provide a generic one for you if you leave it null
        Intent intent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            intent = mKeyguardManager.createConfirmDeviceCredentialIntent("Vérification de sécurité", null);
        }
        if (intent != null) {
            cordova.setActivityResultCallback (this);
            cordova.getActivity().startActivityForResult(intent, REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS);
        } else {
            this.alert("Pour utiliser Pandalab vous devez activer une méthode de devérouillage sécurisé de votre smartphone\n\n" +
            "Pour cela allez dans : \n" +
            "Paramétres -> Sécurité -> Ecran de dévérouillage\n\n" +
            "Ensuite cliquez sur \"c'est bon ! \"", "Alerte de sécurité", "C'est bon !", callbackContext);
        }
    }

    /**
     * Builds and shows a native Android alert with given Strings
     *
     * @param message         The message the alert should display
     * @param title           The title of the alert
     * @param buttonLabel     The label of the button
     * @param callbackContext The callback context
     */
    public synchronized void alert(final String message, final String title, final String buttonLabel, final CallbackContext callbackContext) {
        final CordovaInterface cordova = this.cordova;

        Runnable runnable = new Runnable() {
            public void run() {

                AlertDialog.Builder dlg = createDialog(cordova); // new AlertDialog.Builder(cordova.getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                dlg.setMessage(message);
                dlg.setTitle(title);
                dlg.setCancelable(false);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    dlg.setNeutralButton("Paramètres", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
                            cordova.getActivity().startActivity(intent);
                        }
                    });
                }
                dlg.setPositiveButton(buttonLabel,
                        new AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                testSecure();
                            }
                        });
                changeTextDirection(dlg);
            };
        };
        this.cordova.getActivity().runOnUiThread(runnable);
    }

    private void changeTextDirection(Builder dlg) {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        dlg.create();
        AlertDialog dialog = dlg.show();
        if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            TextView messageview = (TextView) dialog.findViewById(android.R.id.message);
            messageview.setTextDirection(android.view.View.TEXT_DIRECTION_LOCALE);
        }
    }

    private AlertDialog.Builder createDialog(CordovaInterface cordova) {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            return new AlertDialog.Builder(cordova.getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        } else {
            return new AlertDialog.Builder(cordova.getActivity());
        }
    }
}
